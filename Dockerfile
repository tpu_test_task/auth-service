FROM node:19-bullseye-slim

WORKDIR /auth

COPY ./ ./

RUN npm install && npm run build

CMD ["npm", "start"]
