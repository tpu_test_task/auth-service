import {
  Controller,
  Get,
  Post,
  Body,
  UseGuards,
  Request,
  HttpCode,
} from '@nestjs/common';
import { UsersService } from './users/users.service';
import { LocalAuthGuard } from './auth/local/local-auth.guard';
import { JwtAuthGuard } from './auth/jwt/jwt-auth.guard';
import { AuthService } from './auth/auth.service';

@Controller('api')
export class AppController {
  constructor(
    private readonly userService: UsersService,
    private readonly authService: AuthService,
  ) {}

  @Post('signup')
  async signUp(
    @Body() input: { password: string },
  ): Promise<{ id: string; token: string }> {
    const newUser = await this.userService.createUser(input);
    const token = await this.authService.sign(newUser);
    return {
      id: newUser._id.toString(),
      token,
    };
  }

  @UseGuards(LocalAuthGuard)
  @HttpCode(200)
  @Post('signin')
  async signIn(@Request() req): Promise<{ token: string }> {
    const token = await this.authService.sign(req.user);
    return {
      token,
    };
  }

  @Get('user')
  @UseGuards(JwtAuthGuard)
  user(@Request() req) {
    return {
      id: req.user._id,
    };
  }
}
