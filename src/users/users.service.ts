import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Users } from './users.schema';
import { Model } from 'mongoose';
import * as bcrypt from 'bcrypt';

@Injectable()
export class UsersService {
  constructor(@InjectModel(Users.name) private userModel: Model<Users>) {}

  async createUser(createUserDto) {
    const hash = await bcrypt.hash(createUserDto.password, process.env.SALT);
    const createdUser = new this.userModel({
      password: hash,
    });
    await createdUser.save();
    return createdUser;
  }

  findOne(id: string) {
    return this.userModel.findOne({
      _id: id,
    });
  }
}
