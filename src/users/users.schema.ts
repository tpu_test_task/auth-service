import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

@Schema()
export class Users {
  @Prop(String)
  password: string;
}

export const UsersSchema = SchemaFactory.createForClass(Users);
