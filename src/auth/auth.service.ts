import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class AuthService {
  constructor(private jwtService: JwtService) {}

  async sign(user: any) {
    return this.jwtService.sign({ id: user._id });
  }
}
