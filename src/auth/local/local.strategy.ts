import {
  Injectable,
  UnauthorizedException,
  HttpException,
} from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { Users } from '../../users/users.schema';
import { InjectModel } from '@nestjs/mongoose';
import { AuthService } from '../auth.service';
import { Strategy } from 'passport-local';
import { Model } from 'mongoose';
import * as bcrypt from 'bcrypt';

@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy) {
  constructor(
    private authService: AuthService,
    @InjectModel(Users.name) private userModel: Model<Users>,
  ) {
    super();
  }

  async validate(username: string, password: string) {
    const user = await this.userModel.findById(username);

    if (!user) {
      throw new UnauthorizedException();
    }

    const check = await bcrypt.compare(password, user.password);

    if (!check) {
      throw new HttpException('Bad id or password', 401);
    }

    return user;
  }
}
